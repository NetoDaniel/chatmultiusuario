package view;

import java.io.IOException;
import javax.swing.JOptionPane;
import service.ServidorService;

public class ServidorView extends javax.swing.JFrame {
    private ServidorService servidorService;
    private ControleServidor controleServidor;
    private ExibirMensagem exibirMensagem;
    private Thread threadServidor;
    private Thread threadMensagem;
    
    public ServidorView() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelPorta = new javax.swing.JPanel();
        btnConectar = new javax.swing.JButton();
        btnDesconectar = new javax.swing.JButton();
        spnPorta = new javax.swing.JSpinner();
        lblStatus = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Servidor");

        panelPorta.setBorder(javax.swing.BorderFactory.createTitledBorder("Porta"));

        btnConectar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnConectar.setText("Conectar");
        btnConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConectarActionPerformed(evt);
            }
        });

        btnDesconectar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnDesconectar.setText("Desconectar");
        btnDesconectar.setEnabled(false);
        btnDesconectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesconectarActionPerformed(evt);
            }
        });

        spnPorta.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        spnPorta.setModel(new javax.swing.SpinnerNumberModel(1024, 1024, 65535, 1));

        javax.swing.GroupLayout panelPortaLayout = new javax.swing.GroupLayout(panelPorta);
        panelPorta.setLayout(panelPortaLayout);
        panelPortaLayout.setHorizontalGroup(
            panelPortaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPortaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spnPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnConectar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDesconectar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelPortaLayout.setVerticalGroup(
            panelPortaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPortaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPortaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConectar)
                    .addComponent(btnDesconectar)
                    .addComponent(spnPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        lblStatus.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        lblStatus.setText("O servidor está parado");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblStatus)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblStatus)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConectarActionPerformed
        this.actionBtnConectar();
    }//GEN-LAST:event_btnConectarActionPerformed

    private void btnDesconectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesconectarActionPerformed
        this.actionBtnDesconectar();
    }//GEN-LAST:event_btnDesconectarActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ServidorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ServidorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ServidorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ServidorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ServidorView().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConectar;
    private javax.swing.JButton btnDesconectar;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JPanel panelPorta;
    private javax.swing.JSpinner spnPorta;
    // End of variables declaration//GEN-END:variables

    private void actionBtnConectar() {
        this.controleServidor = new ControleServidor();
        this.exibirMensagem = new ExibirMensagem();

        try {
            this.servidorService = new ServidorService((int) spnPorta.getValue());

            this.threadServidor = new Thread(this.controleServidor);
            this.threadServidor.start();

            this.threadMensagem = new Thread(this.exibirMensagem);
            this.threadMensagem.start();
       } catch(IOException ex) {
            JOptionPane.showMessageDialog(null, "Conexão não realizada!\nTente novamente usando uma outra porta.", "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void actionBtnDesconectar() {
        this.threadMensagem.interrupt();
        
        this.threadServidor.interrupt();
        
        this.controleServidor.pararServidor();
        
        this.limparMensagem();
    }
    
    private void limparMensagem() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        spnPorta.setEnabled(true);
        
        btnConectar.setEnabled(true);
        btnDesconectar.setEnabled(false);
        
        lblStatus.setText("O servidor está parado");
        
        JOptionPane.showMessageDialog(null, "Servidor desconectado com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
    }
    
    //CLASSES PRIVADAS
    private class ControleServidor implements Runnable {
        @Override
        public void run() {
            servidorService.start();
        }
        
        public void pararServidor() {
            servidorService.stop();
        }
    }
    
    private class ExibirMensagem implements Runnable {
        @Override
        public void run () {
            setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
            
            spnPorta.setEnabled(false);
            
            btnConectar.setEnabled(false);
            btnDesconectar.setEnabled(true);

            lblStatus.setText("O servidor está em execução");

            JOptionPane.showMessageDialog(null, "Servidor iniciado com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
