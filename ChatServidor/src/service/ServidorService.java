package service;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Mensagem;
import model.Mensagem.Action;

public class ServidorService {
    private ServerSocket serverSocket;
    private Socket socket;
    private Map<String, ObjectOutputStream> mapUsuariosOnline = new HashMap<>();
    private Thread thread;
    
    public ServidorService(int porta) throws IOException {
        this.serverSocket = new ServerSocket(porta);
    }

    public ServerSocket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Map<String, ObjectOutputStream> getMapUsuariosOnline() {
        return mapUsuariosOnline;
    }

    public void setMapUsuariosOnline(Map<String, ObjectOutputStream> mapUsuariosOnlines) {
        this.mapUsuariosOnline = mapUsuariosOnlines;
    }
    
    public void start() {
        while (true) {
            try {
                this.socket = this.serverSocket.accept();
            } catch (IOException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }

            this.thread = new Thread(new OuvinteSocket(socket));
            this.thread.start();
        }
    }
    
    public void stop() {
        try {
            if (this.socket != null) {
                this.socket.close();
            }
            
            this.serverSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // CLASSE PRIVADA
    private class OuvinteSocket implements Runnable {
        private ObjectOutputStream output;
        private ObjectInputStream input;

        public OuvinteSocket(Socket socket) {
            try {
                this.output = new ObjectOutputStream(socket.getOutputStream());
                this.input = new ObjectInputStream(socket.getInputStream());
            } catch (IOException ex) {
                Logger.getLogger(OuvinteSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void run() {
            Mensagem mensagem = null;

            try {
                while ((mensagem = (Mensagem) this.input.readObject()) != null) {
                    Action acao = mensagem.getAcao();

                    switch (acao) {
                        case CONECTAR:
                            boolean isConectado = conectar(mensagem, this.output);
                            if (isConectado) {
                                mapUsuariosOnline.put(mensagem.getNome(), this.output);
                                enviarUsuariosOnlines();
                            }
                            break;
                        case DESCONECTAR:
                            boolean isDesconecatado = desconectar(mensagem);
                            if (isDesconecatado) {
                                mensagem = null;
                            }
                            break;
                        case ENVIAR_UM:
                            enviarParaUm(mensagem);
                            break;
                        case ENVIAR_TODOS:
                            enviarParaTodos(mensagem);
                            break;
                    }
                }
            } catch (IOException ex) {
                if (mensagem != null) {
                    Mensagem mensagemException = new Mensagem();
                    mensagemException.setNome(mensagem.getNome());
                    desconectar(mensagemException);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        private boolean conectar(Mensagem mensagem, ObjectOutputStream output) {
            if (mapUsuariosOnline.isEmpty()) {
                mensagem.setMensagem("YES");
                enviar(mensagem, output);
           
                return true;
            }

            if (mapUsuariosOnline.containsKey(mensagem.getNome())) {
                mensagem.setMensagem("NO");
                enviar(mensagem, output);

                return false;
            } else {
                mensagem.setMensagem("YES");
                enviar(mensagem, output);

                return true;
            }
        }
        
        private boolean desconectar(Mensagem mensagem) {
            mapUsuariosOnline.remove(mensagem.getNome());
            
            mensagem.setMensagem("adeus.");
            mensagem.setAcao(Action.ENVIAR_TODOS);
            enviarParaTodos(mensagem);
            
            enviarUsuariosOnlines();
            
            return true;
        }

        private void enviar(Mensagem mensagem, ObjectOutputStream output) {
            try {
                output.writeObject(mensagem);
            } catch (IOException ex) {
                Logger.getLogger(OuvinteSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        private void enviarParaUm(Mensagem mensagem) {
            for (Map.Entry<String, ObjectOutputStream> entry : mapUsuariosOnline.entrySet()) {
                if (entry.getKey().equals(mensagem.getNomePrivado())) {
                    try {
                        entry.getValue().writeObject(mensagem);
                    } catch (IOException ex) {
                        Logger.getLogger(OuvinteSocket.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
        private void enviarParaTodos(Mensagem mensagem) {
            for (Map.Entry<String, ObjectOutputStream> entry : mapUsuariosOnline.entrySet()) {
                if (!entry.getKey().equals(mensagem.getNome())) {
                    mensagem.setAcao(Action.ENVIAR_UM);
                    try {
                        entry.getValue().writeObject(mensagem);
                    } catch (IOException ex) {
                        Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
        
        private void enviarUsuariosOnlines() {
            Set<String> setNomes = new java.util.HashSet<>();
            for(Map.Entry<String, ObjectOutputStream> entry: mapUsuariosOnline.entrySet()){
                setNomes.add(entry.getKey());
            }
            
            Mensagem mensagem = new Mensagem();
            mensagem.setAcao(Action.USUARIOS_ONLINE);
            mensagem.setClientesOnline(setNomes);
            
            for (Map.Entry<String, ObjectOutputStream> entry : mapUsuariosOnline.entrySet()) {
                mensagem.setNome(entry.getKey());
                try {
                    entry.getValue().writeObject(mensagem);
                } catch (IOException ex) {
                    Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}