package service;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import model.Mensagem;

public class ClienteService {
    private Socket socket;
    private ObjectOutputStream output;
    
    public Socket conectar(String host, int porta) {
        try {
            this.socket = new Socket(host, porta);
            this.output = new ObjectOutputStream(this.socket.getOutputStream());
        } catch (ConnectException ex) {
            JOptionPane.showMessageDialog(null, "Conexão não realizada!\nVerifique se o servidor está ativo.", "Erro!", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(ClienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return this.socket;
    }
    
    public void enviar(Mensagem mensagem) {
        try {
            this.output.writeObject(mensagem);
        } catch (IOException ex) {
            Logger.getLogger(ClienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
