package view;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import model.Mensagem;
import model.Mensagem.Action;
import service.ClienteService;

public class ClienteView extends javax.swing.JFrame {
    private Socket socket;
    private Mensagem mensagem;
    private ClienteService cliente;
    private final String ipServidor;
    private final int portaServidor;

    public ClienteView() {
        initComponents();
        
        this.ipServidor = "localhost";
        this.portaServidor = 8080;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelMensagens = new javax.swing.JPanel();
        scrollHistorico = new javax.swing.JScrollPane();
        txtAreaHistorico = new javax.swing.JTextArea();
        btnLimpar = new javax.swing.JButton();
        btnEnviar = new javax.swing.JButton();
        scrollMensagem = new javax.swing.JScrollPane();
        txtAreaMensagem = new javax.swing.JTextArea();
        panelConectar = new javax.swing.JPanel();
        btnSair = new javax.swing.JButton();
        btnConectar = new javax.swing.JButton();
        txtNome = new javax.swing.JTextField();
        panelOnlines = new javax.swing.JPanel();
        scrollOnlines = new javax.swing.JScrollPane();
        listOnlines = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Cliente");

        panelMensagens.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        txtAreaHistorico.setEditable(false);
        txtAreaHistorico.setColumns(20);
        txtAreaHistorico.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtAreaHistorico.setLineWrap(true);
        txtAreaHistorico.setRows(5);
        txtAreaHistorico.setWrapStyleWord(true);
        txtAreaHistorico.setEnabled(false);
        scrollHistorico.setViewportView(txtAreaHistorico);

        btnLimpar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnLimpar.setText("Limpar");
        btnLimpar.setEnabled(false);
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });

        btnEnviar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnEnviar.setText("Enviar");
        btnEnviar.setEnabled(false);
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        txtAreaMensagem.setColumns(20);
        txtAreaMensagem.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtAreaMensagem.setLineWrap(true);
        txtAreaMensagem.setRows(5);
        txtAreaMensagem.setWrapStyleWord(true);
        txtAreaMensagem.setEnabled(false);
        scrollMensagem.setViewportView(txtAreaMensagem);

        javax.swing.GroupLayout panelMensagensLayout = new javax.swing.GroupLayout(panelMensagens);
        panelMensagens.setLayout(panelMensagensLayout);
        panelMensagensLayout.setHorizontalGroup(
            panelMensagensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMensagensLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelMensagensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollMensagem, javax.swing.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
                    .addComponent(scrollHistorico, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMensagensLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnLimpar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEnviar)))
                .addContainerGap())
        );
        panelMensagensLayout.setVerticalGroup(
            panelMensagensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMensagensLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollHistorico, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(scrollMensagem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelMensagensLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEnviar)
                    .addComponent(btnLimpar))
                .addGap(12, 12, 12))
        );

        panelConectar.setBorder(javax.swing.BorderFactory.createTitledBorder("Conectar"));

        btnSair.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnSair.setText("Sair");
        btnSair.setEnabled(false);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        btnConectar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnConectar.setText("Conectar");
        btnConectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConectarActionPerformed(evt);
            }
        });

        txtNome.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        javax.swing.GroupLayout panelConectarLayout = new javax.swing.GroupLayout(panelConectar);
        panelConectar.setLayout(panelConectarLayout);
        panelConectarLayout.setHorizontalGroup(
            panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConectarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtNome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnConectar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSair)
                .addGap(12, 12, 12))
        );
        panelConectarLayout.setVerticalGroup(
            panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConectarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelConectarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSair)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConectar))
                .addContainerGap())
        );

        panelOnlines.setBorder(javax.swing.BorderFactory.createTitledBorder("Usuários Online"));

        listOnlines.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        listOnlines.setForeground(java.awt.Color.black);
        scrollOnlines.setViewportView(listOnlines);

        javax.swing.GroupLayout panelOnlinesLayout = new javax.swing.GroupLayout(panelOnlines);
        panelOnlines.setLayout(panelOnlinesLayout);
        panelOnlinesLayout.setHorizontalGroup(
            panelOnlinesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOnlinesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollOnlines, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelOnlinesLayout.setVerticalGroup(
            panelOnlinesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelOnlinesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollOnlines)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelMensagens, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelConectar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelOnlines, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelOnlines, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(panelConectar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelMensagens, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConectarActionPerformed
        this.actionBtnConectar();
    }//GEN-LAST:event_btnConectarActionPerformed

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        this.actionBtnSair();
    }//GEN-LAST:event_btnSairActionPerformed

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        this.actionBtnLimpar();
    }//GEN-LAST:event_btnLimparActionPerformed

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        this.actionBtnEviar();
    }//GEN-LAST:event_btnEnviarActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClienteView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClienteView().setVisible(true);
            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConectar;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JButton btnSair;
    private javax.swing.JList<String> listOnlines;
    private javax.swing.JPanel panelConectar;
    private javax.swing.JPanel panelMensagens;
    private javax.swing.JPanel panelOnlines;
    private javax.swing.JScrollPane scrollHistorico;
    private javax.swing.JScrollPane scrollMensagem;
    private javax.swing.JScrollPane scrollOnlines;
    private javax.swing.JTextArea txtAreaHistorico;
    private javax.swing.JTextArea txtAreaMensagem;
    private javax.swing.JTextField txtNome;
    // End of variables declaration//GEN-END:variables

    // MÉTODOS DE AÇÃO
    private void actionBtnConectar() {
        String nome = txtNome.getText();
        
        if (!nome.trim().equals("")) {
            this.mensagem = new Mensagem();
            this.mensagem.setAcao(Action.CONECTAR);
            this.mensagem.setNome(nome);
            
            this.cliente = new ClienteService();
            this.socket = this.cliente.conectar(this.ipServidor, this.portaServidor);
            new Thread(new OuvinteSocket(this.socket)).start();

            this.cliente.enviar(mensagem);
        }
    }

    private void actionBtnSair() {
        Mensagem mensagemLocal = new Mensagem();
        mensagemLocal.setNome(this.mensagem.getNome());
        mensagemLocal.setAcao(Action.DESCONECTAR);
        this.cliente.enviar(mensagemLocal);
        
        this.desconectado();
    }

    private void actionBtnLimpar() {
        txtAreaMensagem.setText("");
    }

    private void actionBtnEviar() {
        String texto = txtAreaMensagem.getText();
        String nome = this.mensagem.getNome();
        
        this.mensagem = new Mensagem();
        
        if (listOnlines.getSelectedIndex() > -1) {
            this.mensagem.setNomePrivado((String) listOnlines.getSelectedValue());
            this.mensagem.setAcao(Action.ENVIAR_UM);
            listOnlines.clearSelection();
        } else {
            this.mensagem.setAcao(Action.ENVIAR_TODOS);
        }
            
        if (!texto.trim().equals("")) {
            this.mensagem.setNome(nome);
            this.mensagem.setMensagem(texto.trim());
            
            txtAreaHistorico.append("Você disse: " + mensagem.getMensagem() + "\n");
            
            this.cliente.enviar(mensagem);
            
            txtAreaMensagem.setText("");
        }
    
    }
    
    // MÉTODOS DO CHAT
    private void conectado(Mensagem mensagem) {
        if (mensagem.getMensagem().equals("NO")) {
            txtNome.setText("");
            JOptionPane.showMessageDialog(this, "Conexão não realizada!\nTente novamente usando um novo nome.", "Erro!", JOptionPane.ERROR_MESSAGE);
            
            return;
        } 
        
        this.mensagem = mensagem;
        
        setTitle(mensagem.getNome());
        
        btnConectar.setEnabled(false);
        txtNome.setEditable(false);        
        btnSair.setEnabled(true);
        
        txtAreaMensagem.setEnabled(true);
        txtAreaHistorico.setEnabled(true);
        btnEnviar.setEnabled(true);
        btnLimpar.setEnabled(true);
        
        JOptionPane.showMessageDialog(this, "Conexão realizada com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private void desconectado() {
        setTitle("Cliente");
        
        btnConectar.setEnabled(true);
        txtNome.setEditable(true);    
        txtNome.setText("");
        btnSair.setEnabled(false);
        
        txtAreaMensagem.setEnabled(false);
        txtAreaMensagem.setText("");
        txtAreaHistorico.setEnabled(false);
        txtAreaHistorico.setText("");
        btnEnviar.setEnabled(false);
        btnLimpar.setEnabled(false);
        
        listOnlines.setListData(new String[0]);
        
        JOptionPane.showMessageDialog(this, "Desconectado com sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private void receberMensagem(Mensagem mensagem) {
        this.txtAreaHistorico.append(mensagem.getNome() + " diz: " + mensagem.getMensagem() + "\n");
    }
    
    private void atualizarUsuarios(Mensagem mensagem) {
        Set<String> nomes = mensagem.getClientesOnline();
        nomes.remove((String) mensagem.getNome());
        String[] array = (String[]) nomes.toArray(new String[nomes.size()]);
        
        listOnlines.setListData(array);
        listOnlines.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listOnlines.setLayoutOrientation(JList.VERTICAL);
    }
    
    // CLASSE PRIVADA
    private class OuvinteSocket implements Runnable {
        private ObjectInputStream input;

        public OuvinteSocket(Socket socket) {
            try {
                this.input = new ObjectInputStream(socket.getInputStream());
            } catch (IOException ex) {
                Logger.getLogger(OuvinteSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void run() {
            Mensagem mensagem = null;

            try {
                while ((mensagem = (Mensagem) this.input.readObject()) != null) {
                    Action acao = mensagem.getAcao();

                    switch (acao) {
                        case CONECTAR:
                            conectado(mensagem);
                            break;
                        case DESCONECTAR:
                            desconectado();
                            socket.close();
                            break;
                        case ENVIAR_UM:
                            receberMensagem(mensagem);
                            break;
                        case USUARIOS_ONLINE:
                            atualizarUsuarios(mensagem);
                            break;
                    }
                }
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(OuvinteSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}