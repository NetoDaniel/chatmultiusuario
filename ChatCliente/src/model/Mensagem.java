package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Mensagem implements Serializable {
    private String nome;
    private String mensagem;
    private String nomePrivado;
    private Set<String> clientesOnline;
    private Action acao;
    
    public Mensagem() {
        this.clientesOnline = new HashSet<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getNomePrivado() {
        return nomePrivado;
    }

    public void setNomePrivado(String nomePrivado) {
        this.nomePrivado = nomePrivado;
    }

    public Set<String> getClientesOnline() {
        return clientesOnline;
    }

    public void setClientesOnline(Set<String> clientesOnline) {
        this.clientesOnline = clientesOnline;
    }

    public Action getAcao() {
        return acao;
    }

    public void setAcao(Action acao) {
        this.acao = acao;
    }
    
    public enum Action {
        CONECTAR, DESCONECTAR, ENVIAR_UM, ENVIAR_TODOS, USUARIOS_ONLINE
    }
}
